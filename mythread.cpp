#include "mythread.h"
#include <QFile>
#include <QProgressDialog>
#include <QMessageBox>
#include <QTextStream>

myThread::myThread( QString& in, QString& out )
{
    inFileName = in;
    outFileName = out;
}

void myThread::run()
{
    QString result;
    QFile inputFile(inFileName);

    if ( inputFile.open(QFile::ReadOnly) )
    {
        emit workStarted();
        QFile outputFile(outFileName);
        outputFile.open(QFile::ReadWrite);
        QTextStream out(&outputFile);
        QString line;

        for ( int i = 0; !inputFile.atEnd() ; i++ )
        {
            if ( isInterruptionRequested() ) {
                inputFile.close();
                outputFile.remove();
                exit();
                return;
            }
            line = inputFile.readLine();
            line.chop(2);
            out << line << "00" << "\n";
        }
        result = "1";
    }
    else
    {
        result = "0";
    }
    emit resultReady(result);
}
