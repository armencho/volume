#ifndef DIALOG_H
#define DIALOG_H

#include "mythread.h"

#include <QDialog>
#include <QProgressDialog>

namespace Ui {
class Dialog;
}

class Dialog : public QDialog
{
    Q_OBJECT

public:
    explicit Dialog(QWidget *parent = 0);
    ~Dialog();

private slots:
        void openClicked();
        void okClicked();
        void handleResults(QString result);
        void cancelRequest();
        void threadWorkStarted();
        void threadFinished();


private:
    QProgressDialog* progress;
    myThread* threadA;
    QString inFileName;
    QString outFileName;
    Ui::Dialog *ui;

};

#endif // DIALOG_H
