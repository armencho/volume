#ifndef MYTHREAD_H
#define MYTHREAD_H

#include <QThread>

class myThread : public QThread
{
    Q_OBJECT
public:
    myThread( QString&, QString& );

protected:
    void run();

signals:
    void resultReady(const QString &result);
    void workStarted();

public slots:

private:
    QString inFileName;
    QString outFileName;

};

#endif // MYTHREAD_H
