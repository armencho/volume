#include "dialog.h"
#include "ui_dialog.h"

#include <iostream>
#include <QFileDialog>
#include <QTextStream>
#include <QMessageBox>

Dialog::Dialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Dialog)
{
    ui->setupUi(this);

    //connections
    connect(ui->openButton, SIGNAL(clicked(bool)),
                this, SLOT(openClicked()));
    connect(ui->buttonBox, SIGNAL(rejected()), this, SLOT(reject()));
    connect(ui->buttonBox, SIGNAL(accepted()), this, SLOT(okClicked()));
}

void Dialog::openClicked()
{
    inFileName = QFileDialog::getOpenFileName(this, tr("Open File"), "", tr("Files (*.txt*)"));
    ui->lineEdit->setText(inFileName);
    outFileName = inFileName;
    outFileName.remove(".txt",Qt::CaseInsensitive);
    outFileName.append("_100.txt");
}

void Dialog::okClicked()
{
    threadA = new myThread(inFileName,outFileName);
    connect(threadA, SIGNAL(resultReady(QString)), this, SLOT(handleResults(QString)));
    connect(threadA, SIGNAL(workStarted()), this, SLOT(threadWorkStarted()));
    connect(threadA, SIGNAL(finished()), this, SLOT(threadFinished()));
    connect(threadA, &myThread::finished, threadA, &QObject::deleteLater);
    threadA->start();
}

void Dialog::threadWorkStarted()
{
    progress = new QProgressDialog(this);
    progress->setModal(true);
    progress->setMinimumSize(300,100);
    progress->setMaximumSize(300,100);
    progress->setRange(0,0);
    progress->setLabelText(tr("Saving %1").arg(outFileName));
    progress->show();
    connect(progress, SIGNAL(canceled()), this, SLOT(cancelRequest()));
}


void Dialog::handleResults(QString result)
{
    if (result == "1")
    {
        delete progress;
        QMessageBox::information(this,"Success","Success!!",QMessageBox::Ok,QMessageBox::NoButton);
    }
    else
    {
        QMessageBox::critical(this,"Error","Bad file Path",QMessageBox::Ok,QMessageBox::NoButton);
    }
}

void Dialog::cancelRequest()
{
    delete progress;
    threadA->requestInterruption();
}

void Dialog::threadFinished()
{
    //std::cout << "HELLO!!!" << "\n";
}

Dialog::~Dialog()
{
    delete ui;
}
